//
//  Service+Utils.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/28/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//

import Alamofire
import Freddy
import Foundation


enum ServiceResult<T> {
    
    case success(responseObject: T)
    case failure(error: String)
    
}

// MARK: - JSON Response Serializer

// convert data to JSON 
extension DataRequest {
    
    static func jsonResponseSerializer() -> DataResponseSerializer<JSON> {
        return DataResponseSerializer(serializeResponse: { (_, response, data, error) in
            
            guard let statusCode = response?.statusCode, (200..<300).contains(statusCode) else {
                return .failure(error!)
            }
            
            let result = Request.serializeResponseData(response: response, data: data, error: error)
            
            guard case .success = result else {
                return .failure(error!)
            }
            
            do {
                let json = try JSON(data: result.value!)
                return .success(json)
            } catch let error {
                return .failure(error)
            }
        })
    }
    
    @discardableResult
    func customResponseJSON(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<JSON>) -> Void) -> Self {
        return validate(statusCode: 200..<300)
            .validate(contentType: ["application/json; charset=UTF-8"]).response(queue: queue, responseSerializer: DataRequest.jsonResponseSerializer(), completionHandler: completionHandler)
    }
    
}

