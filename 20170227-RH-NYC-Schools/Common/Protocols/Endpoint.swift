//
//  Endpoint.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/27/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//
import Alamofire

protocol Endpoint: URLRequestConvertible {
    
    var baseURL: String { get }
    var path: String { get }
    var url: String { get }
    var method: HTTPMethod { get }
    var encoding: ParameterEncoding { get }
    var body: Parameters { get }
    var headers: HTTPHeaders { get }
    
}

extension Endpoint {
    
    var baseURL: String {
        return "https://data.cityofnewyork.us/resource"
    }
    
    var encoding: ParameterEncoding {
        return method == .get ? URLEncoding.default : JSONEncoding.default
    }
    
    var url: String {
        return baseURL + path
    }
    
    var body: Parameters {
        return Parameters()
    }
    
}
