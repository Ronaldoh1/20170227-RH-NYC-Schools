//
//  SchoolListPresenter.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/27/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//

import UIKit

class SchoolListPresenter: NSObject {
    
    private weak var view: SchoolListViewType?
    private var interactor: SchoolListInteractorType?
    private var router: SchoolListRouterType?
    
    private var schoolsDataSource: [School] = []
    
    required init(view: SchoolListViewType, router: SchoolListRouterType, interactor: SchoolListInteractorType) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
    
}

// MARK: SchoolListPresenterType

extension SchoolListPresenter: SchoolListPresenterType {
    
    func viewDidLoad() {
        interactor?.schools(completion: { [weak self] (result) in
            switch result {
            case .success(responseObject: let schools):
                self?.schoolsDataSource = schools
                self?.view?.reloadData()
            case .failure(error: let error): print(error) // here we'd want to let the view to presente an error. ex. view.presenter(errorMessage: error)
            }
        })
    }
    
    func didSelectItem(at indexPath: IndexPath) {
        let school = schoolsDataSource[indexPath.row]
        interactor?.schoolDetails(for: school.id, completion: { [weak self](result) in
            switch result {
            case .success(responseObject: let schoolDetailInfo):
                self?.router?.presentSchoolDetailsViewController(sender: self?.view as! UIViewController, schoolDetailInfo: schoolDetailInfo)
            case .failure(error: let error):
                print(error)
                self?.view?.presentError()
            }
        })
    }
    
}

//MARK: UITableViewDataSource

extension SchoolListPresenter: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolsDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SchoolCell.cellID, for: indexPath) as? SchoolCell else { return UITableViewCell() }
        cell.school = schoolsDataSource[indexPath.row]
        return cell
    }
    
}
