//
//  SchoolListAssembly.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/27/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//

import Swinject
import UIKit

// DI to build the VIPER modules - chose Swinject framework for DI.

class SchoolListAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(SchoolListRouterType.self) { (_) in
            return SchoolListRouter(container: container)
        }
        
        container.register(SchoolListViewType.self) { (_) in
            SchoolListViewController()
            }.initCompleted { (resolver, view) in
                view.presenter = resolver.resolve(SchoolListPresenterType.self)!
        }
        
        container.register(SchoolListPresenterType.self) { (resolver) in
            let view = resolver.resolve(SchoolListViewType.self)!
            let router = resolver.resolve(SchoolListRouterType.self)!
            let interactor = resolver.resolve(SchoolListInteractorType.self)!
            return SchoolListPresenter(view: view, router: router, interactor: interactor)
        }
        
        container.register(SchoolListInteractorType.self) { (resolver) in
            let remoteDataManager = resolver.resolve(SchoolListRemoteDataManagerType.self)!
            return SchoolListInteractor(remoteDataManager: remoteDataManager)
        }
        
        container.register(SchoolListRemoteDataManagerType.self) { (_) in
            return SchoolListRemoteDataManager()
        }
    }
    
}
