//
//  SchoolListRemoteDataManager.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/27/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//
// This class actually makes the service call to the network using alamofire. On success - get some data that gets transformed to JSON. I'm using flatmap on The array of dictionaries to obtain an array of schools. Note that every object maps itself.

import Alamofire

class SchoolListRemoteDataManager { }

extension SchoolListRemoteDataManager: SchoolListRemoteDataManagerType {
    
    func fetchSchoolDetail(for id: String, completion: @escaping (ServiceResult<SchoolDetailsInfo>) -> Void) {
        Alamofire.request(SchoolDataEndpoint.schoolSATDetailInfo(id)).customResponseJSON { (response) in
            switch response.result{
            case .success(let json):
                do {
                    let schoolDetailInfoRaw = try json.getArray()
                    guard schoolDetailInfoRaw.count > 0 else {
                        completion(.failure(error: "There is no available SAT Data for the school selected")) // lets tell the user that there is SAT Data
                        return
                    }
                    let schoolDetailInfo = try schoolDetailInfoRaw.flatMap {
                        try SchoolDetailsInfo(json: $0)
                        }.first!
                    completion(.success(responseObject: schoolDetailInfo))
                } catch let error {
                    completion(.failure(error: error.localizedDescription)) // same just passing localized error for now.
                }
            case .failure(let error):
                completion(.failure(error: error.localizedDescription)) // same just passing localized error for now.
            }
        }
    }
    
    func fetchSchools(completion: @escaping (ServiceResult<[School]>) -> Void) {
        Alamofire.request(SchoolDataEndpoint.schoolList).customResponseJSON { (response) in
            switch response.result {
            case .success(let json):
                do {
                    let schoolsRaw = try json.getArray()
                    let schools = try schoolsRaw.flatMap {
                        try School(json: $0)
                    }
                    completion(.success(responseObject: schools))
                } catch let error {
                    // If I had more time I'd pass the error to the presenter and let it present so that it can notify the view that something went wrong. Using the localized description for now, but this is not ideal
                    completion(.failure(error: error.localizedDescription))
                }
            case .failure(let error):
                completion(.failure(error: error.localizedDescription))
            }
        }
    }
    
}
