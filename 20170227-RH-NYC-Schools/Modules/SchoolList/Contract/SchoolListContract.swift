//
//  SchoolListContract.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/27/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//

import UIKit

// Contract - each class only exposes minimum methods/properties. In order for a class to be of certain type - it must conform to it's required protocol. ex. In order for the viewController to be of type View - it must conform to the SchoolListViewType and implement the necessary properties and methods. 

protocol SchoolListViewType: class {
    
    var presenter: SchoolListPresenterType? { get set }
    
    func reloadData()
    func presentError()
    
}

protocol SchoolListInteractorType: class {
    
    func schools(completion: @escaping (ServiceResult<[School]>) -> Void)
    
    func schoolDetails(for id: String, completion: @escaping (ServiceResult<SchoolDetailsInfo>) -> Void)
    
}

protocol SchoolListPresenterType: class, UITableViewDataSource {
    
    func viewDidLoad()
    func didSelectItem(at indexPath: IndexPath)
    
}

protocol SchoolListRouterType: class {
    
    func presentSchoolList(in window: UIWindow)
    
    func presentSchoolDetailsViewController(sender: UIViewController, schoolDetailInfo: SchoolDetailsInfo)

}

protocol SchoolListRemoteDataManagerType: class {
    
    func fetchSchools(completion: @escaping (ServiceResult<[School]>) -> Void)
    
    func fetchSchoolDetail(for id: String, completion: @escaping (ServiceResult<SchoolDetailsInfo>) -> Void)
    
}

