//
//  SchoolListInteractor.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/27/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//  Notes - here you'd want to persist any data, so it the user kills the app it doesn't have to fetch the same data again.


class SchoolListInteractor {
    
    private var remoteDataManager: SchoolListRemoteDataManagerType?
    
    required init(remoteDataManager: SchoolListRemoteDataManagerType) {
        self.remoteDataManager = remoteDataManager
    }
    
}

extension SchoolListInteractor: SchoolListInteractorType {
    
    func schools(completion: @escaping (ServiceResult<[School]>) -> Void) {
        remoteDataManager?.fetchSchools(completion: { (result) in
            completion(result)
        })
    }
    
    func schoolDetails(for id: String, completion: @escaping (ServiceResult<SchoolDetailsInfo>) -> Void) {
        remoteDataManager?.fetchSchoolDetail(for: id, completion: { (result) in
           completion(result)
        })
    }
    
}
