//
//  SchoolDataEndpoint.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/28/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//

import Alamofire

enum SchoolDataEndpoint {
    
    case schoolList
    case schoolSATDetailInfo(String)
    
}

extension SchoolDataEndpoint: Endpoint {
    
    // Only downloading 30 schools - ideally - this should be paginated, so the user can request the next items if necessary. Downloading 400+ items at once is not a good idea, so i limit it to 30. 
    var path: String {
        switch self {
        case .schoolList: return "/97mf-9njv.json?$limit=30&$offset=0"
        case .schoolSATDetailInfo(let id): return "/734v-jeq5.json?dbn=\(id)"
        }
    }
    
    var parameters: Parameters {
        switch self {
        case .schoolList: return [:]
        case .schoolSATDetailInfo: return [:]
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .schoolList, .schoolSATDetailInfo: return .get
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
        case .schoolList, .schoolSATDetailInfo: return ["":""]
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: baseURL + path)
        var urlRequest = URLRequest(url: url!)
        urlRequest.httpMethod = method.rawValue
        urlRequest.allHTTPHeaderFields = headers
        let encoding = Alamofire.URLEncoding.default
        return try encoding.encode(urlRequest, with: parameters)
    }
    
}
