//
//  SchoolListRouter.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/27/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//

import UIKit
import Swinject

class SchoolListRouter {
    
    private let container: Container
    
    required init(container: Container) {
        self.container = container
    }
    
}

extension SchoolListRouter: SchoolListRouterType {
    
    func presentSchoolList(in window: UIWindow) {
        guard let controller = container.resolve(SchoolListViewType.self) as? SchoolListViewController else { return }
        let rootViewController = UINavigationController(rootViewController: controller)
        window.makeKeyAndVisible()
        window.rootViewController = rootViewController
    }
    
    func presentSchoolDetailsViewController(sender: UIViewController, schoolDetailInfo: SchoolDetailsInfo) {
        guard let router = container.resolve(SchoolDetailsRouterType.self) as? SchoolDetailsRouter else { return }
        router.presentSchoolDetailsViewController(sender: sender, schoolDetailInfo: schoolDetailInfo)
    }
    
}
