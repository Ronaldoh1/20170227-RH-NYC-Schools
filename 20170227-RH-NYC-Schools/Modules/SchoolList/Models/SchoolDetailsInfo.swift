//
//  SchoolDetails.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/28/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//

import Foundation
import Freddy

struct SchoolDetailsInfo {
    
    let id: String
    let numberOfTestTakers: Int
    let averageSATReading: Int
    let averageSATMath: Int
    let averageSATWriting: Int
    let schoolName: String
    
}

// MARK: JSONDecodable

extension SchoolDetailsInfo: JSONDecodable {
    
    init(json: JSON) throws {
        id = try json.getString(at: "dbn")
        numberOfTestTakers = try json.getInt(at: "num_of_sat_test_takers")
        averageSATReading = try json.getInt(at: "sat_critical_reading_avg_score")
        averageSATMath = try json.getInt(at: "sat_math_avg_score")
        averageSATWriting = try json.getInt(at: "sat_writing_avg_score")
        schoolName = try json.getString(at: "school_name")
    }
    
}
