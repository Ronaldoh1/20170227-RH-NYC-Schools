//
//  School.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/27/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//

import Freddy

struct School {
    
    let id: String
    let name: String
    let neighborhood: String?
    
}

// MARK: JSONDecodable


extension School: JSONDecodable {
    
    init(json: JSON) throws {
        id = try json.getString(at: "dbn")
        name = try json.getString(at: "school_name")
        neighborhood = try json.getString(at: "neighborhood", alongPath: .missingKeyBecomesNil)
    }
    
}
