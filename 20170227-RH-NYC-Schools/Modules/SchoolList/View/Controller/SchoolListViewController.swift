//
//  SchoolListViewController.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/27/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//

import UIKit
import SnapKit

class SchoolListViewController: UIViewController {
    
    var presenter: SchoolListPresenterType?
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.delegate = self
        tableView.dataSource = self.presenter
        tableView.allowsSelection = true
        tableView.showsVerticalScrollIndicator = true
        tableView.register(SchoolCell.self, forCellReuseIdentifier: SchoolCell.cellID)
        tableView.separatorInset = .zero
        return tableView
    }()
    
    //MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        navigationItem.title = "NYC Schools"
        presenter?.viewDidLoad()
        setupViews()
    }
    
    //MARK: - Setup Views
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        guard traitCollection != previousTraitCollection else { return }
        
        switch traitCollection.verticalSizeClass {
        case .regular: setupRegularConstraints()
        case .compact, .unspecified: break
        }
    }
    
    private func setupViews() {
        view.addSubview(tableView)
    }
    
    private func setupRegularConstraints() {
        tableView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
    
    
}

// MARK: UITableViewDelegate

extension SchoolListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        presenter?.didSelectItem(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (40 / 667 * UIScreen.main.bounds.height)
    }
    
}

// MARK: SchoolListViewType

extension SchoolListViewController: SchoolListViewType {
    
    func reloadData() {
        tableView.reloadData()
    }
    
    func presentError() {
        let alertController = UIAlertController(title: "No SAT Data ", message: "Unfortunately, we could not find SAT data for school selected ", preferredStyle: .alert)
        let OkAction = UIAlertAction(title: "OK", style: .default) { (action: UIAlertAction) in
      
        }
        alertController.addAction(OkAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
}
