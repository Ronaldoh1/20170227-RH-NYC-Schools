//
//  SchoolCell.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/28/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//

import UIKit

class SchoolCell: UITableViewCell {
    
    static let cellID = "SchoolCell"
    
    private let schoolNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .left
        label.tag = 1
        return label
    }()
    
    private let neighborhoodLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textAlignment = .left
        label.tag = 2
        return label
    }()

    var school: School? {
        didSet {
            guard school != nil else { return }
            schoolNameLabel.text = school?.neighborhood
            neighborhoodLabel.text = school?.neighborhood
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setUpViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Setup Views
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        guard traitCollection != previousTraitCollection else { return }
        switch traitCollection.verticalSizeClass {
        case .regular: setupRegularConstraints()
        case .compact, .unspecified: break
        }
    }
    
    private func setUpViews() {
        addSubview(schoolNameLabel)
        addSubview(neighborhoodLabel)
    }
    
    private func setupRegularConstraints() {
        schoolNameLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview()
            make.top.equalToSuperview().offset(5)
            make.height.equalTo(20)
        }
        
        neighborhoodLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10 / 667 * UIScreen.main.bounds.height)
            make.right.equalToSuperview()
            make.top.equalTo(schoolNameLabel.snp.bottom)
            make.height.equalTo(15)
        }
    }

}
