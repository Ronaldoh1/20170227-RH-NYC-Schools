//
//  SchoolDetailsViewController.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/28/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//

import UIKit

class SchoolDetailsViewController: UIViewController {
    
    var presenter: SchoolDetailsPresenterType?
    
    private let schoolNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = .blue
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        return label
    }()
    
    private let numberOfTestTakersLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textAlignment = .center
        return label
    }()
    
    private let SATReadingScoreLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textAlignment = .center
        return label
    }()
    
    private let SATMathScoreLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textAlignment = .center
        return label
    }()
    
    private let SATWritingScoreLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textAlignment = .center
        return label
    }()
    
    private lazy var labelsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [schoolNameLabel, numberOfTestTakersLabel, SATReadingScoreLabel, SATMathScoreLabel, SATWritingScoreLabel])
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.backgroundColor = .red
        return stackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        setupViews()
        presenter?.viewDidLoad()
        
    }
    
    // MARK: Setup Views
    
    private func setupViews() {
        view.addSubview(labelsStackView)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        guard traitCollection != previousTraitCollection else { return }
        
        switch traitCollection.verticalSizeClass {
        case .regular: setupRegularConstraints()
        case .compact, .unspecified: break
        }
    }
    
    private func setupRegularConstraints() {
        labelsStackView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.left.equalToSuperview().offset(15)
            make.right.equalToSuperview().offset(-15)
            make.height.equalTo(200)
        }
    }
    
}

extension SchoolDetailsViewController: SchoolDetailsViewType {
    
    func updateView(with schoolDetailsInfo: SchoolDetailsInfo) {
        schoolNameLabel.text = schoolDetailsInfo.schoolName
        numberOfTestTakersLabel.text = "Number of Test Takes: \(schoolDetailsInfo.numberOfTestTakers)"
        SATReadingScoreLabel.text = "Average SAT Reading Scores: \(schoolDetailsInfo.averageSATReading)"
        SATMathScoreLabel.text = "Average SAT Math Scores: \(schoolDetailsInfo.averageSATMath)"
        SATWritingScoreLabel.text = "Average SAT Writing Scores: \(schoolDetailsInfo.averageSATWriting)"
    }
    
}
