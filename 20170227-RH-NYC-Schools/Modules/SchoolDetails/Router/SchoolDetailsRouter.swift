//
//  SchoolDetailsRouter.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/28/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//

import Swinject
import UIKit

class SchoolDetailsRouter {
    
    private let container: Container
    
    required init(container: Container) {
        self.container = container
    }
    
}

extension SchoolDetailsRouter: SchoolDetailsRouterType {
    
    func presentSchoolDetailsViewController(sender: UIViewController, schoolDetailInfo: SchoolDetailsInfo) {
        guard let controller = container.resolve(SchoolDetailsViewType.self) as? SchoolDetailsViewController else {
            return
        }
        controller.presenter?.schoolDetailsInfo = schoolDetailInfo
        sender.navigationController?.pushViewController(controller, animated: true)
    }
    
}
