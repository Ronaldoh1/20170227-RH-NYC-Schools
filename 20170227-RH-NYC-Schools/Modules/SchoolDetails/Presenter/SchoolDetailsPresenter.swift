//
//  SchoolDetailsPresenter.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/28/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//

import Foundation

class SchoolDetailsPresenter {
    
    private var view: SchoolDetailsViewType?
    private var router: SchoolDetailsRouterType?
    
    var schoolDetailsInfo: SchoolDetailsInfo?
    
    required init(view: SchoolDetailsViewType, router: SchoolDetailsRouterType) {
        self.view = view
        self.router = router
    }
    
}

extension SchoolDetailsPresenter: SchoolDetailsPresenterType {
    
    func viewDidLoad() {
        view?.updateView(with: schoolDetailsInfo!)
    }
    
}
