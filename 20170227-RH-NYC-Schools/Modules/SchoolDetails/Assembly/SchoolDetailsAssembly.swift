//
//  SchoolDetailsAssembly.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/28/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//

import Swinject
import UIKit

class SchoolDetailsAssembly: Assembly {
    
    func assemble(container: Container) {
        container.register(SchoolDetailsRouterType.self) { (_) in
            return SchoolDetailsRouter(container: container)
        }
        
        container.register(SchoolDetailsViewType.self) { (_) in
            SchoolDetailsViewController()
            }.initCompleted { (resolver, view) in
                view.presenter = resolver.resolve(SchoolDetailsPresenterType.self)!
        }
        
        container.register(SchoolDetailsPresenterType.self) { (resolver) in
            let view = resolver.resolve(SchoolDetailsViewType.self)!
            let router = resolver.resolve(SchoolDetailsRouterType.self)!
            return SchoolDetailsPresenter(view: view, router: router)
        }
    }
    
}
