//
//  SchoolDetailsContract.swift
//  20170227-RH-NYC-Schools
//
//  Created by Ronald Hernandez on 2/28/18.
//  Copyright © 2018 Ronaldoh1. All rights reserved.
//

import UIKit

protocol SchoolDetailsViewType: class {
    
    var presenter: SchoolDetailsPresenterType? { get set }
    
    func updateView(with schoolDetailsInfo: SchoolDetailsInfo)
    
}

protocol SchoolDetailsPresenterType: class {
    
    var schoolDetailsInfo: SchoolDetailsInfo? { get set }
    
    func viewDidLoad()
    
}

protocol SchoolDetailsRouterType: class {
    
 func presentSchoolDetailsViewController(sender: UIViewController, schoolDetailInfo: SchoolDetailsInfo)
    
}
